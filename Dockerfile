FROM tomcat:9.0

COPY ./target/animoz*.war /usr/local/tomcat/webapps/animoz.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
